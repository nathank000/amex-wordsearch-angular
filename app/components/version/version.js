'use strict';

angular.module('searchApp.version', [
  'searchApp.version.interpolate-filter',
  'searchApp.version.version-directive'
])

.value('version', '0.1');
