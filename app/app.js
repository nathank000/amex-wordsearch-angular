'use strict';

// Declare app level module which depends on views, and components
var searchApp = angular.module('searchApp', [
  'ngRoute',
  'searchApp.wordSearch',
  'searchApp.pastSearches',
  'searchApp.version'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/wordSearch'});
}]);