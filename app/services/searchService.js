searchApp.service('searchService', [function() {
	var searchSvc  = {
		
		searches: -1,
		
		init: function() {
			console.log('search service initted');
			if (searchSvc.searches === -1) {
				searchSvc.searches = [];
			}
		},
		
		
		saveSearch: function(searchObject) {
			console.log('save search called');
			searchSvc.searches.push(searchObject);
			console.log('searches in searchSvc = ', searchSvc);
		},
		
		getSearches: function() {
			console.log('get searches called');
			var currentSearches = searchSvc.searches; 
			return currentSearches;
		}
		
	};
	
	return searchSvc;
}]);