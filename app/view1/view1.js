'use strict';

angular.module('searchApp.wordSearch', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/wordSearch', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$scope', 'searchService', function($scope, searchService) {
	
	searchService.init();
	
	$scope.contentToSearch = 'The George Washington Bridge in New York City is one of the oldest bridges ever constructed. It is now being remodeled because the bridge is a landmark. City officials say that the landmark bridge effort will create a lot of new jobs in the city.';
	$scope.searchContent = function() {
		
		//reset
		$scope.segmentResults = [];
				
		//console.log('searching the content, e = ' +inputBox);
		//var inputVal = inputBox.value.trim().toLowerCase();
		var inputVal = $scope.search.words.trim().toLowerCase();
		
		console.log('input value = ' + inputVal);
		
		//console.log("space found " +inputVal);
		//showNoResults();
		//removePreviousMatches();
		
		
		var words = $scope.contentToSearch.toLowerCase().trim().split(" ");
		var search = inputVal.split(" ");
		$scope.notFound = [];
		
		console.log("words = ", words);
		console.log("search = ", search);
		
		$scope.searchResults = new Object();
		
		for (var s = 0; s<search.length; s++) {
			var thisSearch = search[s];
			$scope.searchResults[thisSearch] = $scope.findWordPlacement(thisSearch, words);
			
			if ($scope.searchResults[thisSearch] === -1) {
				$scope.notFound.push(thisSearch);
			}
		}
		
		console.log('resulting object = ', $scope.searchResults);
		
		if ($scope.notFound.length == 0) {
			$scope.wordsMatch($scope.searchResults);
		}
	};

	$scope.findWordPlacement = function(word, arrayToSearch) {
				var wordLocations = [];
				
				for (var i=0; i<arrayToSearch.length; i++) {
					//console.log("searching for " +word +" in the array");
					
					//console.log('comparign ' +arrayToSearch[i] +' to ' +word);
					//regex. we meet again.
					if (arrayToSearch[i].match(/[a-z]+/gi) == word) {
						console.log("found, returning " +i);
						wordLocations.push(i);
					}
				}
				
				
				if (wordLocations.length >= 1) {
					return wordLocations;
				}
				
				console.log('not found, returning -1');
				return -1;
		};
		
		$scope.wordsMatch = function(matchObj) {
				//find the item in the array that has the fewest matches
				var pivot = $scope.findLowestMatches(matchObj);
				
				
				console.log('pivot key = ' + pivot +' with ' +matchObj[pivot].length +' number of occurances' );
				console.log('pivot initial location = ' + matchObj[pivot][0]);
								
				
				for (var p=0; p<matchObj[pivot].length; p++) {
				//just dealing with the first pivot location
					var wordsLowerKey = matchObj[pivot][p];
					var wordsHighestKey = matchObj[pivot][p];
					
					 
					for(var foo in matchObj){
						if (foo != pivot) {
							//loop through the positions of matchObj[foo] and find the lowest one from the pivot
							var keyWithLowestDist = $scope.findLowestAbsDistance(matchObj[pivot][p], matchObj[foo]);
							console.log('lowest distance is ' +keyWithLowestDist +'with a value of ' +matchObj[foo][keyWithLowestDist]);
							
							if (matchObj[foo][keyWithLowestDist] < wordsLowerKey) {
								console.log(matchObj[foo][keyWithLowestDist] + ' is LESS than wordsLowerKey at ' +wordsLowerKey);
								wordsLowerKey = matchObj[foo][keyWithLowestDist];
							}
							
							if (matchObj[foo][keyWithLowestDist] > wordsHighestKey) {
								console.log(matchObj[foo][keyWithLowestDist] + ' is GREATER than than wordsLowerKey at ' +wordsHighestKey);
								wordsHighestKey = matchObj[foo][keyWithLowestDist];
							}
						}
					}
					
					console.log('range is now ' +wordsLowerKey +' to ' +wordsHighestKey);
					
					
					//commented out from riginal
					//var holder = document.getElementById('resultsHolder');
					var tempSegment = $scope.contentToSearch.trim().split(" ");
					
					var segment = tempSegment.slice(wordsLowerKey, wordsHighestKey+1).join(' ');
					
					var segmentLength = (wordsHighestKey - wordsLowerKey)+1;
	 
					$scope.segmentResults.push({
											'terms': $scope.search.words.trim().toLowerCase(),
											'lowestSegmentKey':wordsLowerKey, 
											'highestSegmentKey':wordsHighestKey,
											'segmentLength': segmentLength,
											'segment': segment
											
										});
					
					//hideNoResults();
				}
				
				var lowestSegment = -1;
				var lowestSegmentLength = 0;
				for(var s=0; s<$scope.segmentResults.length; s++) {
					//console.log('lowest segment = ' + lowestSegment);
					//console.log('this scopes segment length = ' +segmentMatches[s].segmentLength);
					if (($scope.segmentResults[s].segmentLength<lowestSegmentLength) || (lowestSegment == -1)) {
						//console.log('setting the lowestSegment to = ' + s);
						lowestSegment = s;
						lowestSegmentLength = $scope.segmentResults[s].segmentLength;
					}
				}				
				$scope.segmentResults[lowestSegment]['lowest'] = true;
				searchService.saveSearch($scope.segmentResults[lowestSegment]);
				
				
				////////////////////////////////////////////////////////////////
				//assuming all of the results are shown - this highlites the segment that is of the lowest count
				////////////////////////////////////////////////////////////////
				//highliteLowest(lowestSegment);
				console.log('segment matches = ', $scope.segmentResults);
		};
		
		$scope.findLowestMatches = function(matchElementObject) {
				var lowest = -1;
				var lowKey = '';
				console.log('finding lowest for ', matchElementObject +' with a length of ' +Object.keys(matchElementObject).length);
				
				for (var prop in matchElementObject) {
					
					console.log('looking for the length of the prop ' +prop + ' which is ' + matchElementObject[prop].length);
					
					if ((lowest == -1) || (matchElementObject[prop].length < lowest)) {
						console.log('setting the lowest');
						lowest = matchElementObject[prop].length;
						lowKey = prop;
					}
				}
				return lowKey;
		};
		
		$scope.findLowestAbsDistance = function(pivotPosition, testPositionsArray) {
				//added var here - correct?
				var lowestPos = -1;
				var lowestKey = '';
				for (var l =0; l < testPositionsArray.length; l++) {
					
					var thisPos =  testPositionsArray[l];
					//console.log('findLowestAbsDistance:: thisPos = ' + thisPos);
					
					var absDist = Math.abs(pivotPosition - thisPos);
					//console.log('findLowestAbsDistance:: absDist = ' + absDist);
					
					if ((absDist < lowestPos) || (lowestPos == -1)) {
						lowestPos = absDist;
						lowestKey = l;
					}
				}
				
				console.log('findLowestAbsDistance:: returning = ' + lowestKey);
				return lowestKey;
		};

}]);