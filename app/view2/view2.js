'use strict';

angular.module('searchApp.pastSearches', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/pastSearches', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', ['$scope', 'searchService', function($scope, searchService) {
	searchService.init();
	$scope.pastSearches = searchService.getSearches();
}]);